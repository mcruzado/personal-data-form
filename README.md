# Personal Data Form

Dockerized app for try different form validations

## Project Description

Personal Data Form with different validators

## Special Validations

    1) Entity type: You can select 'Natural' for Physical person or 'Legal' for Legal entities.
    2) Document type: You can select type of document ('NIF/NIE', 'CIF', 'Passport'). If Legal entity is selected, you will be able only to select CIF. On the other hand, if you choose 'Natural', you will be able to select 'NIF/NIE' or 'Passport'.
    3) Identification Number: Form contains a complex validation to allow only NIF/NIEs with the correct letter and length.
    3) Postal Code: Using the library postcode-validator, it's possible to check if Postal Code format matches with selected country. If country code is unrecognized, any postal code will be validated.
    4) Email: Only valid email formats are allowed. Example: foo@bar.com
    5) Phone: Telephone numbers with or without country prefix are valid. Example: 34667712241, 667712241, 952307070
    6) Address: Only addresses with the correct format are allowed. Example: Street Noname 123, Av Fake S/N
    7) Surname: This field only appears if we select 'Natural' entity.
    8) Name, Surname, City: Standard validation for allow only letters, spaces and accents
    9) Country: Countries are imported from a data file. One of them needs to be selected

## Technical Requirements to run the application:

  - Docker: https://docs.docker.com/get-docker/ 
  - (If you have only installed docker for command line and you have not installed docker for desktop, you will need also install 'docker-compose' and set it up)
  
## Features

    - All components are made with React Hooks/Functional programming.
    - A lite version of an API REST is included with the app. You can test validation and add some accounts to the list. By default, database contains 2 registered accounts.
    - API will check if the document entered is currently saved in the database. In that case, we cannot save the account.
    - Accounts are displayed in JSON format in different text-areas on form's right side.
    - Responsive Design.
    - Custom field validators.
    - Countries and entities data are located in /client/src/data.
    - Server config are located in /client/src/config/server.json.

## Technologies

    - Front-End:

      · Node: 14.15.0
      · React: ^17.0.1
      · Chakra-UI (Styled components for React): ^0.8.0
      · react-hook-form (Form library for React Hooks): ^6.11.0
      · postcode-validator: ^3.1.1
      · axios (Http client): ^0.21.0

    - Back-End:

      · Node: 14.15.0
      · Express: ^4.17.1
      · Sequelize (ORM): ^6.3.5
      · Sqlite3 (Embedded DataBase): ^5.0.0

    - Deployment:

      · Docker

## Repository

  - git clone https://gitlab.com/mcruzado/personal-data-form.git

## Run

    - Option 1:   
        make run
    - Option 2:   
        docker-compose up -d
        