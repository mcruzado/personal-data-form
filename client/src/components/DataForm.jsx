import React from 'react';
import { useForm } from 'react-hook-form';
import { FormControl, FormLabel, Input, FormErrorMessage, Box, Button, RadioGroup, Radio, Divider, Select, Stack, Text } from '@chakra-ui/core';
import { validateAddress, validateCIF, validateEmail, validateName, validateNIForNIE, validatePassport, validatePhone, validatePostCode } from '../tools/validators';
import Countries from '../data/countries.json';
import Entities from '../data/entities.json';
import Documents from '../data/documents.json';
import ServerConfig from '../config/server.json';
import axios from 'axios';

//Document validator matrix
const documentValidator = {
  validateNIForNIE: validateNIForNIE,
  validateCIF: validateCIF,
  validatePassport: validatePassport,
};

//Component
const DataForm = (props) => {
  const { handleSubmit, register, errors, formState, getValues } = useForm();
  const [document, setDocument] = React.useState('1');
  const [entity, setEntity] = React.useState('1');
  const [country, setCountry] = React.useState('');
  const [submitError, setSubmitError] = React.useState('');

  const documentTypeHandler = (event) => {
    if (event.target.value) {
      setDocument(event.target.value);
    };
  };

  const entityTypeHandler = (event) => {
    if (event.target.value) {
      setEntity(event.target.value);
      if (event.target.value === "2") {
        setDocument("2");
        if (errors.surname) {
          delete errors['surname'];
        };
      } else {
        setDocument("1");
      };
    };
  };

  const countryHandler = (event) => {
    if (event.target.value) {
      setCountry(event.target.value);
    };
  };

  const prepareData = (data) => {
    let preparedData = {
      ...data,
      document: Documents[document].name,
      entity: Entities[entity].name,
    };
    for (let key in preparedData) {
      preparedData[key] = preparedData[key].trim();
    };
    return preparedData;
  };

  const submitNewAccount = async (data) => {
    try {
      const dataToPost = prepareData(data);
      const buildHttp = `${ServerConfig.defaultServer}:${ServerConfig.defaultPort}/api/accounts`;
      const response = await axios.post(buildHttp, dataToPost);
      if (response.status === 200) {
        const newAccount = response.data;
        props.add(newAccount);
        setSubmitError('');
      };
    } catch (e) {
      let error;
      if (e.message === 'Request failed with status code 400') {
        error = 'You have entered a duplicated document!';
      } else {
        error = 'Internal server error!';
      }
      setSubmitError(`*${error}`);
    };
  };

  const findCountryCode = (countryName) => country ? Countries[Countries.findIndex((country) => country.name === countryName)].code : null;

  return (
    <Box maxW="lg" padding={5} borderWidth={1} margin={1}>
      <form onSubmit={handleSubmit(data => submitNewAccount(data))}>
        <Stack spacing={1}>
          <FormControl>
            <RadioGroup onChange={entityTypeHandler} value={entity} isInline={true} margin={2} name="entity">
              {Object.keys(Entities).map((key) => (
                <Radio value={key} key={key}>{Entities[key].name}</Radio>
              ))}
            </RadioGroup>
          </FormControl>
          <Divider />
          <FormControl>
            <Stack isInline>
              <FormLabel htmlFor="document">
                Document:
            </FormLabel>
              <FormControl>
                <RadioGroup onChange={documentTypeHandler} value={document} isInline={true} name="document">
                  {Object.keys(Documents).map((key) => (
                    <Radio key={key} value={key} isDisabled={entity === Documents[key].disabledIfEntity}>{Documents[key].name}</Radio>
                  ))
                  }
                </RadioGroup>
              </FormControl>
            </Stack>
          </FormControl>
          <FormControl isInvalid={errors.identificationCode}>
            <Input
              maxLength="15"
              name="identificationCode"
              type="text"
              placeholder="Enter ID"
              ref={register({ validate: documentValidator[Documents[document].validator] })}
            />
            <FormErrorMessage>
              {errors.identificationCode && (errors.identificationCode.message)}
            </FormErrorMessage>
          </FormControl>
          <Divider />
          <FormControl isInvalid={errors.country}>
            <FormLabel htmlFor="country">
              Country
        </FormLabel>
            <Select placeholder="Select country" onChange={countryHandler} value={country} name="country" ref={register({ required: 'Please select a country' })}>
              {
                Countries.map((country) => (
                  <option key={country.code} value={country.name}>{country.name}</option>
                ))
              }
            </Select>
            <FormErrorMessage>
              {errors.country && (errors.country.message)}
            </FormErrorMessage>
          </FormControl>

          <FormControl isInvalid={errors.address}>
            <FormLabel htmlFor="address">Address</FormLabel>
            <Input
              name="address"
              placeholder="Enter address"
              ref={register({ validate: validateAddress })}
            />
            <FormErrorMessage>
              {errors.address && (errors.address.message)}
            </FormErrorMessage>
          </FormControl>
          <Stack isInline maxWidth="100%">
            <FormControl isInvalid={errors.city} width="100%">
              <Input
                name="city"
                placeholder="Enter city"
                ref={register({ validate: () => validateName(getValues('city'), 'city') })}
              />
              <FormErrorMessage>
                {errors.city && (errors.city.message)}
              </FormErrorMessage>
            </FormControl>
            <FormControl isInvalid={errors.postcode} maxWidth="25%">
              <Input
                name="postcode"
                maxLength={8}
                type="text"
                placeholder="Enter Postcode (Example: 29000)"
                ref={register({ validate: () => validatePostCode(getValues('postcode'), findCountryCode(country)) })}
              />
              <FormErrorMessage>
                {errors.postcode && (errors.postcode.message)}
              </FormErrorMessage>
            </FormControl>
          </Stack>

          <FormControl isInvalid={errors.name}>
            <FormLabel htmlFor="name">Name</FormLabel>
            <Input
              name="name"
              placeholder="Enter name"
              ref={register({ validate: () => validateName(getValues('name'), 'name') })}
            />
            <FormErrorMessage>
              {errors.name && (errors.name.message)}
            </FormErrorMessage>
          </FormControl>
          {/* Surname (only for natural person) */}
          {entity === "1" && (<FormControl isInvalid={entity === "1" && (errors.surname)}>
            <FormLabel htmlFor="surname">Last name</FormLabel>
            <Input
              name="surname"
              placeholder="Enter last name"
              ref={register({ validate: entity === "1" ? () => validateName(getValues('surname'), 'last name') : true })}
            />
            <FormErrorMessage>
              {errors.surname && (errors.surname.message)}
            </FormErrorMessage>
          </FormControl>)}

          <FormControl isInvalid={errors.email}>
            <FormLabel htmlFor="email">
              Email
        </FormLabel>
            <Input
              name="email"
              placeholder="Enter email"
              ref={register({ validate: validateEmail })}
            />
            <FormErrorMessage>
              {errors.email && (errors.email.message)}
            </FormErrorMessage>
          </FormControl>
          <FormControl isInvalid={errors.telephone}>
            <FormLabel htmlFor="phone">
              Phone
        </FormLabel>
            <Input
              maxLength="15"
              name="telephone"
              type="tel"
              placeholder="Enter phone"
              ref={register({ validate: validatePhone })}
            />
            <FormErrorMessage>
              {errors.telephone && (errors.telephone.message)}
            </FormErrorMessage>
          </FormControl>
          <Button
            isLoading={formState.isSubmitting}
            mt={3}
            variantColor="teal"
            type="submit"
          >Save</Button>
          {setSubmitError && (<Text color="red.500" fontSize={14}>{submitError}</Text>)}
        </Stack>
      </form>
    </Box>
  );
};

export default DataForm;
