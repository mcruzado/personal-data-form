import { Textarea, Stack } from '@chakra-ui/core';
import React from 'react';

const DataViewer = (props) => (!props.data || props.data.length === 0 ? null :
  <Stack spacing={2} maxW="lg" padding={5} borderWidth={1} margin={1} overflowY='auto' maxH="100vh">
      {props.data.map((row) => (
        <Textarea isDisabled={true} size="md" key={row.id} value={JSON.stringify(row)} />
      ))}
  </Stack>
);

export default DataViewer;
