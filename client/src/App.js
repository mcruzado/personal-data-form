import DataForm from './components/DataForm';
import './App.css';
import { ThemeProvider } from 'emotion-theming';
import { theme, ColorModeProvider, CSSReset, Stack, Flex } from '@chakra-ui/core';
import axios from 'axios';
import { useEffect, useState } from 'react';
import DataViewer from './components/DataViewer';
import ServerConfig from './config/server.json';

const App = () => {
  const [data, setData] = useState([]);

  //Retrieving data from API when components load
  useEffect(() => retrieveData(), []);

  const retrieveData = async() => {
    try {
      const buildHttp = `${ServerConfig.defaultServer}:${ServerConfig.defaultPort}/api/accounts`;
      const response = await axios.get(buildHttp);
      if (response.status === 200 && response.data) {
        const accounts = response.data;
        setData(accounts);
      };
    } catch (e) {
      console.error(e);
    };
  };

  //Function to pass to children for adding new accounts
  const dataAppender = (newData) => {
    data.push(newData);
    setData([...data]);
  };

  //Render
  return (
    <ThemeProvider theme={theme}>
      <ColorModeProvider>
      <Flex width="full" align="center" justifyContent="center" margin={2}>
        <Stack spacing={5} isInline>
        <DataForm add={dataAppender} />
        <DataViewer data={data}></DataViewer>
        </Stack>
        </Flex>
        <CSSReset />
      </ColorModeProvider>
    </ThemeProvider>
  );
};

export default App;
