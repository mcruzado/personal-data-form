import { postcodeValidator, postcodeValidatorExistsForCountry } from 'postcode-validator';

export const validateName = (name, type) => {
  let error;
  const reg = /^[ a-zA-ZÀ-ÿ\u00f1\u00d1]*$/g;
  if (name.length < 3 || !name.match(reg)) {
    error = `Please enter a valid ${type}`;
  } else {
    if (!name) {
      error = `Please enter a ${type}`;
    }
  }
  return error || true;
};

export const validateEmail = (email) => {
  let error;
  const reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (!email.match(reg)) {
    error = "Please enter a valid email";
  }
  return error || true;
};

export const validatePhone = (phone) => {
  let error;
  const reg = /^[0-9\-\+]{9,15}$/;
  if (!phone.match(reg)) {
    error = "Please enter a valid phone number";
  };
  return error || true;
};

export const validateNIForNIE = (document) => {
  let error;
  const nif = document.toUpperCase();
  const reg = /^[XYZ]?\d{5,8}[A-Z]$/;
  const nifLetters = 'TRWAGMYFPDXBNJZSQVHLCKET';

  if (nif.match(reg)) {
      const number = nif.substr(0,nif.length-1).replace('X',0).replace('Y', 1).replace('Z',2);
      const letter = nif.substr(nif.length - 1, 1);
      const numberDivision = number % 23;

      if (letter !== nifLetters.substring(numberDivision, numberDivision+1)) {
        error = 'Letter does not match with the document';
      };
  } else {
      error = 'Invalid NIF format';
  };
  return error || true;
};

export const validateCIF = (document) => {
  let error;
  const reg = /^[a-zA-Z]{1}\d{7}[a-zA-Z0-9]{1}$/;
  if (!document.match(reg)) {
    error = 'Invalid CIF format';
  };
  return error || true;
};

export const validatePassport = (document) => {
  let error;
  const reg = /^[A-PR-WYa-pr-wy][1-9]\d\s?\d{4}[1-9]$/;
  if (!document.match(reg)) {
    error = 'Invalid passport format';
  };
  return error || true;
};

export const validatePostCode = (code, countryCode) => {
  let error;
  if (!countryCode) {
    error = 'Please select a country';
  }
  if (postcodeValidatorExistsForCountry(countryCode) && !postcodeValidator(code, countryCode)) {
    error = 'PC does not match to the country selected';
  };
  return error || true
};

export const validateAddress = (address) => {
  let error;
  const reg = /[,#-\/\s\!\@\$.....]/gi;
  if (!address.match(reg)) {
    error = 'Invalid address format';
  };
  return error;
};