const messages = {
  badRequest: 'Missing content. Cannot be empty',
  serverNotWorking: 'Internal server error',
  alreadyExists: 'Account with the same document already exists!',
  customError: (id) => `Account with id ${id} cannot be retrieved`, 
};

module.exports = messages;
