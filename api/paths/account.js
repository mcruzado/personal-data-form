const routes = (app) => {
  const router = require('express').Router();
  const controller = require('../controllers/account');

  router.post('/', controller.new);
  router.get('/', controller.list)
  router.get('/:id', controller.findById);

  app.use('/api/accounts', router);
};

module.exports = routes;
