const database = require('../models');
const Account = database.accounts;
const Responses = require('../messages/responses');
const isEmpty = require('lodash').isEmpty;
const controller = {};

controller.new = async (req, res) => {
  if (isEmpty(req.body)) {
    res.status(400).send({ message: Responses.badRequest })
  } else {
    try {
      const accExists = await Account.findOne({
        where: {
          identificationCode: req.body.identificationCode,
        }
      });
      if (accExists) {
        res.status(400).send({ message: Responses.alreadyExists })
      } else {
        const account = {
          name: req.body.name,
          ...(req.body.surname && {
            surname: req.body.surname,
          }),
          ...(req.body.country && {
            country: req.body.country
          }),
          document: req.body.document,
          identificationCode: req.body.identificationCode,
          entity: req.body.entity,
          ...(req.body.profession && {
            profession: req.body.profession
          }),
          ...(req.body.city && {
            city: req.body.city
          }),
          ...(req.body.address && {
            address: req.body.address
          }),
          ...(req.body.postcode && {
            postcode: req.body.postcode
          }),
          telephone: req.body.telephone,
          email: req.body.email,
        };
        const accToCreate = await Account.create(account);
        res.send(accToCreate);
      }
    } catch (e) {
      res.status(500).send({ message: Responses.serverNotWorking });
    };
  };
};

controller.list = async (_req, res) => {
  try {
    const accounts = await Account.findAll();
    res.send(accounts);
  } catch (e) {
    res.status(500).send({ message: Responses.serverNotWorking });
  };
};

controller.findById = async (req, res) => {
  if (!req.params.id) {
    res.status(400).send({ mesage: Responses.badRequest });
  } else {
    try {
      const account = await Account.findByPk(req.params.id);
      if (!account) {
        res.status(404).send({ mesage: Responses.customError(req.params.id) });
      }
    } catch (e) {
      res.status(500).send({ mesage: Responses.serverNotWorking });
    };
  };
};

module.exports = controller;
