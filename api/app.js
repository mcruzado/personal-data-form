const express = require('express');
const app = express();
const parser = require('body-parser');
const database = require('./models');

const cors = require('cors');

const PORT = process.env.PORT || 8080;

const encodeOpt = {
	extended: true,
};
const defaultRes = {
	message: 'Personal Data Form API. Ready for requests!',
};

app.use(cors());
app.use(parser.json());
app.use(parser.urlencoded(encodeOpt));

//Syncing sequelize db
database.sequelize.sync();

//Routes
app.get('/', (_req, res) => res.json(defaultRes));
require('./paths/account')(app);

//Port
app.listen(PORT, () => {
	console.log(`Server running on ${PORT}.`);
});
