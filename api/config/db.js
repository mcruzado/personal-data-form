//Authentication & setup for mysql database
dbConfig = {
  dialect: 'sqlite',
  storage: './data/db.sqlite',
};

module.exports = dbConfig;
