const Sequelize = require('sequelize');
const dbConfig = require('../config/db');

//Creating Sequelize session
const sequelize = new Sequelize(dbConfig);

//Creating db
const database = {};

database.Sequelize = Sequelize;
database.sequelize = sequelize;

//Creating model
database.accounts = require('./account')(sequelize, Sequelize);

module.exports = database;
