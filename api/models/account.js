const Account = (seq, Seq) => {
  return seq.define('account', {
    entity: {
      type: Seq.STRING,
      allowNull: false,
    },
    country: Seq.STRING,
    name: {
      type: Seq.STRING,
      allowNull: false,
    },
    surname: Seq.STRING,
    document: {
      type: Seq.STRING,
      allowNull: false,
    },
    identificationCode: {
      type: Seq.STRING,
      allowNull: false,
    },
    city: Seq.STRING,
    address: Seq.STRING,
    postcode: Seq.INTEGER,
    profession: Seq.STRING,
    email: {
      type: Seq.STRING,
      allowNull: false,
    },
    telephone: {
      type: Seq.INTEGER,
      allowNull: false,
    }
  });
};

module.exports = Account;
